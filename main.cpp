#include <iostream>

#include <libs/memory-handler/memory-handler.h>
#include <libs/flog/flog.h>


/*
* Ka�dy obiekt musi dziedziczy� po Fabr::FaberObject najlepiej wirtualnie 
* 
* Memory handler jest bez sensu skoro i tak korzystamy z std::shared i std::unique
* 
* Napisa�em bibliotek� do loggowania : 
* FERR - error
* FWAR - warning
* FINF - info
* 
* na argumenciew mog� miec w sumie wszystko co jest jakos stringiem, a ��czysz +
* 
* FERR("Zmienna jaka� tam:" + zmienna);
* 
* FASSERT - asercja
* pierwszy argument to wyra�enie true / false kt�re uwa�asz �e w danym punkcie programu jest aksiomem, musi byc prawdziwe
* je�li nie jest to wywala program - u�ywaj tego jak najwi�cej. Mo�esz tam wrzuci� wszystko to co do ifa tak na prawd�: 
* 
* FASSERT(condition)
* FASSERT(value == 5)
* FASSERT(a + b > 8)
* itp itd
* 
* Masz jeszcze FASSERT_MSG(condition, "msg")
* 
* wszystkie logi z tych makr zapisuj� si� do pliku kt�rego �cie�k� masz w flog.h
* 
*/

class A : public Fabr::Memory::MemoryObject
{
public:
	A() { std::cout << "A()" << std::endl; }
	~A() { std::cout << "~A()" << std::endl; }
	virtual void foo() { std::cout << "A::foo()" << std::endl; }
};

class B : virtual public A
{
public:
	B() { std::cout << "B()" << std::endl; }
	~B() { std::cout << "~B()" << std::endl; }
	void foo() { std::cout << "B::foo()" << std::endl; }

};

int main()
{
	std::shared_ptr<B> obj;
	{
		Fabr::Memory::MemoryHandler hander;
		obj = hander.createObject<B>();
		obj->foo();
	}
	int variable_test = 4;

	FERR("macro TEST");
	FWAR("warning test");
	FWAR("condition : " << variable_test);
	FINF("condition : " << variable_test);
	FASSERT(variable_test == 3);
	FASSERT_MSG(variable_test == 2, "msg");
	

	std::cout<<"end" << std::endl;
	return 0;
}