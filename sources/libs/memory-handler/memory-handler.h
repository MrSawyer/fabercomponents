#pragma once
/*
* Nasze biblioteki
*/
#include <engine.h>
#include <libs/memory-handler/memory-object.h>
/*
* C++'owe biblioteki
*/
#include <memory>
#include <set>
#include <string>

namespace Fabr::Memory
{
	class MemoryHandler 
	{
	public:
		template <class T>
		std::shared_ptr<T> createObject();
		void deleteObject(MemoryObject * object);
	protected:

	private:
		std::set<std::shared_ptr<MemoryObject>> memory_content_;
	};


	template<class T>
	inline std::shared_ptr<T> MemoryHandler::createObject()
	{
		std::shared_ptr<T> object = std::make_shared<T>();
		std::pair<std::set<std::shared_ptr<MemoryObject>>::iterator, bool> ret = memory_content_.insert(object);
		return object;
	}
}// namespace Fabr::Memory