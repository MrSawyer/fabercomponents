#pragma once
#include <engine.h>

#include <iostream>


namespace Fabr::Memory
{
	class MemoryObject : virtual public Fabr::FaberObject
	{
	public:
		MemoryObject() { std::cout << "MemoryObject()" << std::endl; }
		MemoryObject(const MemoryObject&) = default;
		MemoryObject(MemoryObject&&) = default;
		MemoryObject & operator=(const MemoryObject&) = default;
		MemoryObject& operator=(MemoryObject&&) = default;
		virtual ~MemoryObject() { std::cout << "~MemoryObject()" << std::endl; };
	protected:

	private:

	};
}