#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>
#include <windows.h>
#include <sstream>      // std::stringstream

// uncomment to disable assert()
// #define NDEBUG

namespace Fabr::Flog
{

	const std::string LOG_FILE_PATH = "logs.txt";

	class Logger {
	public:
		Logger(Logger& other) = delete;
		Logger(const Logger&) = delete;
		Logger(Logger&&) = delete;
		Logger& operator=(const Logger&) = delete;
		Logger& operator=(Logger&&) = delete;

		~Logger();
		
		static Logger& get();

		void writeLogToFile(const std::stringstream & msg);
	
	private:
		Logger();

		std::fstream file;
	};

	void printError(const std::stringstream & msg, unsigned int color);

#define MAKE_LOG(lv, msg, color)\
	do {\
		std::stringstream log; \
		std::string relative_path(__FILE__); \
		if(relative_path.find("fabercomponents") != std::string::npos) relative_path = relative_path.substr(relative_path.find("fabercomponents")); \
		log << lv << "[" << relative_path << "][" << __func__ << "()](" << std::to_string(__LINE__) << ") " << msg << "\n"; \
		Fabr::Flog::printError(log, color); \
		Fabr::Flog::Logger::get().writeLogToFile(log); \
	}while(0);\


#define FASSERT_MSG(condition, msg)\
	do { \
		if (condition) break; \
		MAKE_LOG("ASSERT", "<"<< #condition <<" : False> "<<msg, 12);	\
		Fabr::Flog::Logger::get().~Logger(); \
		std::abort(); \
	} while (0); \

#define FASSERT(condition)FASSERT_MSG(condition, "")


#define FERR(msg) MAKE_LOG("ERR", msg, 12);


#define FWAR(msg) MAKE_LOG("WAR", msg, 14);


#define FINF(msg) MAKE_LOG("INF", msg, 10);

}