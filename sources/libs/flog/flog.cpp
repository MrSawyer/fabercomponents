#include <libs/flog/flog.h>

namespace Fabr::Flog 
{

	void Logger::writeLogToFile(const std::stringstream & msg)
	{

		if (file.good())file << __TIME__ << " " << __DATE__ << " | " << msg.str();
		else
		{
			std::stringstream ss;
			ss << "Cannot write to log file\n";
			std::cout << " good()=" << file.good();
			std::cout << " eof()=" << file.eof();
			std::cout << " fail()=" << file.fail();
			std::cout << " bad()=" << file.bad();
			printError(ss, 12);
		}
	}

	Logger::Logger()
	{
		file.open(LOG_FILE_PATH, std::fstream::out | std::fstream::app);
		if (!file.good())
		{
			FERR("Cannot open Log file  : " << LOG_FILE_PATH);
		}
	}

	Logger::~Logger()
	{
		file.close();
	}

	Logger& Logger::get()
	{
		static Logger instance_;
		return instance_;
	}


	void printError(const std::stringstream & log, unsigned int color)
	{
		static CONSOLE_SCREEN_BUFFER_INFO Info;
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &Info);
		static WORD Attributes = Info.wAttributes;

		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (WORD)color);
		std::cerr << log.rdbuf();
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), Attributes);
	}

}//namespace Fabr::Flog